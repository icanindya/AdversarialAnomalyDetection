package com.icanindya.adversarial

object Constants {
  val trainFile = "E:/Data/KDD99/scld_ben_train"
  val testFile = "E:/Data/KDD99/scld_test"
  val attrFile = "E:/Data/KDD99/attributes"
  val numClusters = 100
  val sqDistThres = 0.3
  val numAttr = 41
}